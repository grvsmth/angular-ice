var iceApp = angular.module('iceApp');

iceApp.controller('corporaList', ['$scope', '$rootScope', '$sce', 'Corpora', 'CorpusNames', function($scope, $rootScope, $sce, Corpora, CorpusNames) {

	    $scope.corpora = Corpora;
	    $scope.corpusNames = CorpusNames;
	    $scope.newName = '';
	    $scope.showDelete = false;
	    $scope.showRename = false;
	    $scope.processStatus = '';
	    $scope.processName = '';
	    $scope.cTitle = '';
	    $scope.cText = '';

	    $scope.cTitleOptions = {
		'cw': "Count Words",
		'fp': "Find Patterns"
	    };

	    $scope.corpusStatus = { NOT_PROCESSED : "Not Processed" , PROCESSING : "Processing..." , PROCESSED : "Processed" };

	    $scope.iString = function(indices) {
		return Object.keys(indices).join(', ');
	    };

	    $scope.$on('Corpora.update', function(event) {
		    $scope.corpora = Corpora;
		});

	    $scope.$on('CorpusNames.update', function(event) {
		    $scope.corpusNames = CorpusNames;
		});

	    $scope.rename = function() {
		$scope.showRename = false;
		Corpora.rename($scope.newName);
		$scope.newName = '';
	    };

	    $scope.delete = function() {
		$scope.showDelete = false;
		Corpora.delete();
	    };

	    $scope.confirmProcess = function(processName) {
		Corpora.confirmProcess(processName);

	    };

	    $scope.process = function(processName) {
		Corpora.process(processName);
	    };

	    $scope.renameFeedback = $sce.trustAsHtml("✘");
	    $scope.renameFieldChange = function() {
		if($scope.renameCorpusForm.$valid) {
		    $scope.renameFeedback = $sce.trustAsHtml("✔");
		}
	    };

	}]);

iceApp.controller('corpusDirs', ['$scope', '$rootScope', '$sce', 'Dirs', 'Corpora', 'Focus', function($scope, $rootScope, $sce, Dirs, Corpora, Focus) {

	    $scope.addCorpusName = '';
	    $scope.addCorpusDir = '';
	    $scope.addCorpusFilter = '';

	    $scope.dirs = Dirs;
	    
	    $scope.$on('Dirs.update', function(event) {
		    $scope.dirs = Dirs;
		    Focus('corpusNew');
		});

	    $scope.formFeedback = $sce.trustAsHtml("✘");
	    $scope.fieldChange = function() {
		if($scope.newInputs.$valid) {
		    $scope.formFeedback = $sce.trustAsHtml("✔");
		}
	    };

	    $scope.submit = function() {
		Corpora.add($scope.addCorpusName, Dirs.data.selected, $scope.addCorpusFilter);
		$scope.addCorpusName = '';
		$scope.addCorpusFilter = '';
	    };
	}]);

iceApp.controller('corpusNamesList', ['$scope', '$rootScope', 'CorpusNames', function($scope, $rootScope, CorpusNames) {

	    $rootScope.iceData.bg = '';
	    $scope.corpusNames = CorpusNames;

	    if (null == $scope.corpusNames.list) {
		$scope.corpusNames.list = [];
	    }

	    $scope.$on('CorpusNames.update', function(event) {
		    $scope.corpusNames = CorpusNames;
		    CorpusNames.getForeground();
		});

	    $scope.$on('gotFG', function() {
		    CorpusNames.getBackground();
		});

	    $scope.saveBackground = function(corpusName) {
		console.log("saveBackground(" + corpusName + ")");
		CorpusNames.saveBackground(corpusName).then( function() {
			CorpusNames.getForeground();
		    });
	    };

	}]);

iceApp.controller('entitiesList', ['$scope', '$rootScope', '$location', 'Entities', 'CorpusNames', function($scope, $rootScope, $location, Entities, CorpusNames) {

	    $rootScope.iceData.bg = '';
	    $scope.corpusNames = CorpusNames;
	    $scope.entities = Entities;
	    $scope.notfg = '!';
	    $scope.fg = $location.search().foregroundCorpus;
	    $scope.modalHeight = 400;

	    if (null != $scope.fg) {
		$rootScope.iceData.fg = $scope.fg;
	    } else {
		if ($rootScope.iceData.fg == null || $rootScope.iceData.fg.length < 1) {
		    CorpusNames.loadSelected();
		}
		$scope.notfg = '!' + $rootScope.iceData.fg;
	    }

	    if (null == $scope.corpusNames.list) {
		$scope.corpusNames.list = [];
	    }

	    $scope.filterEntity = function(element) {
		return $rootScope.types[element.type].checked;
	    }

	    $scope.$on('CorpusNames.update', function(event) {
		    $scope.corpusNames = CorpusNames;
		});

	    $scope.$on('Entities.update', function(event) {
		    $scope.entities = Entities;
		});

	    $scope.saveBackground = function(corpusName) {
		console.log("saveBackground(" + corpusName + ")");
		CorpusNames.saveBackground(corpusName).then( function() {
			Entities.getList();
		    });
	    };

	    $scope.init = function() {
		CorpusNames.getList();
		CorpusNames.getBackground().then ( function() {
			Entities.getList();
		    });
	    };


	}]);

iceApp.controller('operationsList', ['$scope', '$rootScope', '$timeout', 'Operations', function($scope, $rootScope, $timeout, Operations) {

  $scope.operations = Operations;
  $scope.active = false;

  $scope.$on('Ops.update', function() {
    $scope.operations = Operations;
    if ($scope.operations.data.pendingTotal > 0 && $scope.active == true) {
			
	$timeout( function() {
		Operations.getList();
	    }, 2000);
    }
      });

  $scope.init = function() {
      $scope.active = true;
      Operations.getList();
  };

  $scope.nextUrl = function(jobId) {
      var url = '';
      if (jobId in $scope.operations.data.completed) {
	  var jobName = $scope.operations.data.completed[jobId].name;
	  var corpusName = $scope.operations.data.completed[jobId].corpus;
	  var nextPage = $scope.operations.nextStep[jobName].page;
	  if('viewEntities' == nextPage) {
	      nextPage += '.html?foregroundCorpus=' + corpusName;
	  } else {
	      nextPage += '.html';
	  }
      }
      return nextPage;
  }

	}]);

iceApp.controller('indicesList', ['$scope', '$rootScope', '$http', '$sce', 'Indices', 'CorpusNames', function($scope, $rootScope, $http, $sce, Indices, CorpusNames) {
	    $scope.indices = Indices;
	    $scope.iceData = $rootScope.iceData;
	    $scope.active = false;
	    $scope.type="noun";
	    $scope.cutoff=3.0;
	    $scope.formValid = false;

	    $scope.activate = function() {
		$scope.active = true;
	    };

	    $scope.$on('Indices.update', function() {
		    $scope.indices = Indices;
		});
	    $scope.$on('gotBG', function() {
		    if ($scope.active) {
			Indices.getList();
		    }
		});

	    $scope.createIndex = function() {
		Indices.add($rootScope.iceData.fg, $rootScope.typeCode[$scope.type], $scope.cutoff);
		$rootScope.iceData.showAddIndexModal=false;
	    }

	    $scope.cutoffFeedback = $sce.trustAsHtml("✘");
	    $scope.cutoffChange = function() {
		if ($scope.cutoff > 0 && $scope.buildIndexModalType) {
		    $scope.formValid = true;
		    $scope.cutoffFeedback = $sce.trustAsHtml("✔");
		}
	    };

	    $scope.entityTypeChange = function() {
		if ($scope.cutoff > 0) {
		    $scope.cutoffFeedback = $sce.trustAsHtml("✔");
		    $scope.formValid = true;
		}
	    };

	    $scope.delete = function() {
		$rootScope.iceData.showDeleteIndexModal=false;
		Indices.delete();
	    };

	}]);

iceApp.controller('entitySetsList', ['$scope', '$rootScope', '$sce', '$window', '$timeout', 'EntitySets', 'Indices', 'Focus', function($scope, $rootScope, $sce, $window, $timeout, EntitySets, Indices, Focus) {
      $scope.entitySets = EntitySets;
      $scope.showSuggest = false;
      $scope.newName = '';
      $scope.indices = {};
      $scope.modalHeight = 500;
      $scope.rankContainerHeight = 450;
      $scope.waitModalTitle = "Please wait...";
      $scope.ranking = {};
      $scope.showExportSuccess = false;
      $scope.showExpandwaitModal = false;
      $scope.expandProgress = 0;
      $scope.newEntityClass = "glyphicon-remove";
      $scope.newEntityDivClass = "has-error";
      $scope.entityValid = false;

      $scope.$on('EntitySets.update', function() {
	      $scope.entitySets = EntitySets;
	      if (EntitySets.data.selected) {
		  Focus('newEntity');
		  if (Indices.data.selected) {
		      $scope.showSuggest = true;
		  }
	      }
	      if (EntitySets.data.selected && $scope.entityForm.$valid) {
		  $scope.entityValid = true;
	      }
	  });
      
      $scope.$on('Indices.update', function() {
	      $scope.indices = Indices;
	      if (Indices.data.selected && EntitySets.data.selected) {
		  $scope.showSuggest = true;
	      }
	  });
      
      $scope.$on('EntitySets.expanded', function() {
	      $scope.entitySets = EntitySets;
	      $scope.showExpandWaitModal = false;
	      $scope.showRankEntitiesModal = true;
	      Focus('rankEntities_0_u');
	  });
      
      $scope.$on('EntitySets.abort', function() {
	      $scope.showWaitModal = false;
		    // ABG TODO: add notification alert?
	  });

      nextRow = function(i) {
	  if (i < EntitySets.data.expansion.length) {
	      var next = i+1;
	      var nf = 'rankEntities_' + next + '_u';
	      Focus(nf);
	  }
      }

      $scope.rankKey = function(e, entity) {
	  e.preventDefault();
	  var i = parseInt(e.target.name);
	  if (e.key == 'ArrowDown') {
	      nextRow(i);
	  } else if ( e.key == 'u' || e.key == 
		      'Enter') {
	      EntitySets.data.ranking[entity] = 'U';
	      nextRow(i);
	  } else if (e.key == 'y' || ' ' == e.key) {
	      $scope.entitySets.data.ranking[entity] = 'Y';
	      nextRow(i);
	  } else if ('n' == e.key) {
	      EntitySets.data.ranking[entity] = 'N';
	      nextRow(i);
	  } else if (e.key == 'p' || 'ArrowUp' == e.key) {
	      if (i > 0) {
		  var prev = i-1;
		  var nf = 'rankEntities_' + prev + '_u';
		  Focus(nf);
	      }
	  } else if (e.keyCode == '116') {
	      $window.location.reload(true);
	  }
	  
      };

      $scope.nameFeedback = $sce.trustAsHtml("✘");
      $scope.nameFieldChange = function() {
	  if($scope.addEntitySetForm.$valid) {
	      $scope.newEntitySetClass = "glyphicon-ok";
	      $scope.newEntitySetDivClass = "has-success";
	  } else {
	      $scope.newEntitySetClass = "glyphicon-remove";
	      $scope.newEntitySetDivClass = "has-error";
	  }
      };
      
      $scope.entityNameFeedback = $sce.trustAsHtml("✘");
      $scope.newEntityChange = function() {
	  if (EntitySets.data.selected) {
	      $scope.entityValid = true;
          }
      };

      $scope.showAddIndex = function() {
	  $rootScope.iceData.showAddIndexModal = true;
	  Focus('indexCutoff');
      };

      $scope.showAddEntitySet = function() {
	  $scope.showAddEntitySetModal = true;
	  Focus('entitySetName');
      };
      
      $scope.add = function() {
	  $scope.showAddEntitySetModal = false;
	  EntitySets.add($scope.newName);
	  $scope.newName = '';
      };
      
      $scope.delete = function() {
	  $scope.showDeleteEntitySetModal = false;
	  EntitySets.delete();
      };

      $scope.expand = function(indexName) {
	  $scope.selectedIndex = indexName;
	  $scope.waitModalTitle = "Expanding entity set...";
	  $scope.showWaitModal = true;
	  $scope.modalHeight = Math.round($window.innerHeight * 0.8);
	  $scope.rankContainerHeight = Math.round($window.innerHeight * 0.4);
	  EntitySets.expand(indexName);
      };

      $scope.addEntity = function() {
	  EntitySets.addEntity($scope.newEntity);
	  $scope.newEntity = "";
	  $scope.entityValid = false;
      };
      
      $scope.rank = function() {
	  EntitySets.rank($scope.selectedIndex);
	  $scope.showWaitModal = true;
	  $scope.showRankEntitiesModal = false;
      };
      
      $scope.saveRanking = function() {
	  $scope.showRankEntitiesModal = false;
	  EntitySets.saveRanking();
      };
      
      $scope.$on('EntitySets.expanding', function() {
	$scope.showExpandWaitModal = true;
	$scope.showWaitModal = false;
	$scope.expandStatus = EntitySets.data.expandStatus;
	if ($scope.expandStatus.progress < 100 && EntitySets.data.expander.length > 0) {
	    $timeout( function() {
		    EntitySets.expandStatus();
		}, 500);
	} else {
	    $timeout( function() {
		    EntitySets.getRanked();
		}, 500);
	}
	  });


      $scope.$on('EntitySets.exported', function() {
	      $scope.showExportSuccess = true;
	      $timeout( function() {
		      $scope.showExportSuccess = false;
		  }, 2000);
	  });
      
      $scope.download = function() {
	  console.info(EntitySets.data.downloadURL);
	  window.open(EntitySets.data.downloadURL);
      };

	}]);

iceApp.controller('relationsList', ['$scope', '$rootScope', '$timeout', '$window', 'Relations', 'RelationInfo', 'Focus', function($scope, $rootScope, $timeout, $window, Relations, RelationInfo, Focus) {
	    $scope.relations = Relations;
	    $scope.showWaitModal = false;
	    $scope.showRankPatternsModal = false;
	    $scope.modalHeight = 500;
	    $scope.waitModalTitle = '';
	    $scope.active = false;

	    $scope.$on('Relations.update', function() {
		    $scope.relations = Relations;
		});

	    $scope.$on('gotBG', function() {
		    if ($scope.active) {
			Relations.getList();
		    }
		});

	    $scope.$on('Relations.expanded', function() {
		    $scope.modalHeight = Math.round($window.innerHeight * 0.8);
		    $scope.relations = Relations;
		    $scope.showWaitModal = false;
		    $scope.waitModalTitle = '';
		    $scope.showRankPatternsModal = true;
		    Focus('rankPatterns_0_u');
		});

	    $scope.$on('Relations.exported', function() {
		    $scope.showExportSuccess = true;
		    $timeout( function() {
			    $scope.showExportSuccess = false;
			}, 2000);
		});

	    $scope.$on('Relations.abort', function() {
		    $scope.showWaitModal = false;
		    // ABG TODO: add notification alert?
		});

 	    nextRow = function(i) {
		if (i < Relations.data.expansion.length) {
		    var next = i+1;
		    var nf = 'rankPatterns_' + next + '_u';
		    Focus(nf);
		}
	    }

	    $scope.rankKey = function(e, pattern) {
		e.preventDefault();
		var i = parseInt(e.target.name);
		if (e.key == 'ArrowDown') {
		    nextRow(i);
		} else if ( e.key == 'u' || e.key == 
'Enter') {
		    Relations.data.expansion[i].userAssess = 'u';
		    nextRow(i);
		} else if (e.key == 'y' || ' ' == e.key) {
		    Relations.data.expansion[i].userAssess = 'y';
		    nextRow(i);
		} else if ('n' == e.key) {
		    Relations.data.expansion[i].userAssess = 'n';
		    nextRow(i);
		} else if (e.key == 'p' || 'ArrowUp' == e.key) {
		    if (i > 0) {
			var prev = i-1;
			var nf = 'rankPatterns_' + prev + '_u';
			Focus(nf);
		    }
		} else if (e.keyCode == '116') {
		    $window.location.reload(true);
		}
		
	    };

	    $scope.select = function(relationName) {
		Relations.data.selected = relationName;
		RelationInfo.get(relationName);
	    };

	    $scope.showAllPatterns = function() {
		Relations.data.selected = null;
		RelationInfo.getPatterns('');
	    };

	    $scope.showSententialPatterns = function() {
		Relations.data.selected = null;
		RelationInfo.getPatterns("sentential");
	    };

	    $scope.delete = function() {
		$scope.showDelete=false;
		Relations.delete();
	    };

	    $scope.showRenameModal = function() {
		$scope.showRename = true;
		Focus('newRelationName');
	    };

	    $scope.showAddToRelationModal = function() {
		$rootScope.iceData.showAddToRelationModal = true;
		Focus('addToRelationName');
	    };

	    $scope.removePath = function() {
		console.log(RelationInfo.data);
		Relations.removePath(RelationInfo.data.selected.path);
	    };

	    $scope.rename = function() {
		$scope.showRename = false;
		Relations.rename($scope.newRelationName);
	    };

	    $scope.expand = function() {
		$scope.waitModalTitle = 'Expanding relation...';
		$scope.showWaitModal = true;
		Relations.expand();
	    };

	    $scope.rank = function() {
		Relations.rank($scope.selectedIndex);
		$scope.waitModalTitle = 'Re-ranking relation...';
		$scope.showWaitModal = true;
		$scope.showRankPatternsModal = false;
	    };

	    $scope.saveRanking = function() {
		$scope.showRankPatternsModal = false;
		Relations.saveRanking();
	    };

	    $scope.cancelRanking = function() {
		$scope.showRankPatternsModal = false;
		Relations.select(Relations.data.selected);
	    };

	    $scope.download = function() {
		downloadURL = Relations.data.downloadURL;
		console.info(downloadURL);
		window.open(downloadURL);
	    };

	    $scope.firstLine = function(htmlText) {
		var fline = htmlText.match(">(.+?)<");
		if (null != fline) {
		    return fline[0];
		} else {
		    return htmlText;
		}
	    }

	}]);

iceApp.controller('relationInfo', ['$scope', '$rootScope', '$sce', 'Relations', 'RelationInfo', function($scope, $rootScope, $sce, Relations, RelationInfo) {
	    $scope.relationInfo = RelationInfo;
	    $scope.relations = Relations;
	    $scope.showAll = true;
	    $scope.required = false;
	    $scope.targetRelation = "";
	    $scope.newName = "";

	    $scope.$on('Relations.update', function() {
		    $scope.relations = Relations;
		    if (Relations.data.selected != null && Relations.data.selected.length > 0) {
			console.log("Relations.update: " + Relations.data.selected);
			RelationInfo.get(Relations.data.selected);
		    }
		});

	    $scope.$on('RelationInfo.update', function() {
		    $scope.relationInfo = RelationInfo;
		});

	    $scope.addToRelationFeedback = $sce.trustAsHtml("✘");
	    $scope.addToRelationFieldChange = function() {
		if($scope.addToRelationForm.$valid) {
		    $scope.targetRelation = "";
		    $scope.addToRelationFeedback = $sce.trustAsHtml("✔");
		}
	    };

	    $scope.addToRelationSelect = function(relation) {
		$scope.newName = "";
		$scope.addToRelationFeedback = $sce.trustAsHtml("✔");
	    };

	    $scope.addToRelation = function() {
		if ($scope.newName.length > 0) {
		    $scope.targetRelation = $scope.newName;
		    $rootScope.iceData.showAddToRelationModal=false;
		    Relations.addPattern($scope.targetRelation, RelationInfo.data.selectedPattern.completePattern);
		}
	    };

	}]);

iceApp.filter('nofg', function() {
	    return function(corpusName, existingName) {
		console.log(corpusName + "=?" + existingName);
		return corpusName === existingName;
	    };
	});
