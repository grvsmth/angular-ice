var iceApp = angular.module('iceApp', [])
    .config(['$httpProvider', '$locationProvider', function($httpProvider, $locationProvider) {
	    $httpProvider.defaults.withCredentials = true;
	    $locationProvider.html5Mode(true);
	    }]);

iceApp.run(['$rootScope', function($rootScope) {
	    $rootScope.sURL = 'http://linserv2.cims.nyu.edu:19720/';
	    $rootScope.iceData = {};

	    $rootScope.types = {
		'nn': { 'name': 'Noun', 'sname': 'nn', 'checked': true },
		'nnp': { 'name': 'Name', 'sname': 'nnp', 'checked': true },
		'vb': { 'name': 'Verb', 'sname': 'vb', 'checked': true },
		'o': { 'name' : 'Other', 'sname': 'o', 'checked': true } };

	    $rootScope.typeCode = {
	        'Noun': 'nn',
		'Name': 'nnp',
		'Verb': 'vb',
		'Other': 'o'
	    };

	    $rootScope.cmd = {};

	    $rootScope.popOver = {
		'index' : 'To make the process efficient, ICE keeps an index of the words appearing at least n times in a given context. Before you build the first entity set from a new corpus, you have to build this index.'
	    };

	}]);

iceApp.directive('setForeground', ['$rootScope', function($rootScope) {
	    return {
		link: function (scope, element, attrs) {
		    element.bind('click', function() {
			    console.log("d setForeground(" + scope.corpusName + ")");
			    $rootScope.iceData.fg = scope.corpusName;
			});
		}
	    };
    }]);

iceApp.directive('corpusTable', function() {
	console.log("corpusTable");
	return {
	    template: '<td>{{ corpora }}</td>'
	};
    });

iceApp.directive('fgCorpus', ['$rootScope', 'CorpusNames', function($rootScope, CorpusNames) {
	    return {
		compile: function () {
		    if (null == $rootScope.iceData.fg) {
			CorpusNames.getForeground();
		    }
		},
		template: '<div class="badge iceCorpusBadge unselectableText">Foreground Corpus: {{ iceData.fg }}</div>'
	    }
	}]);

iceApp.directive('bgCorpus', ['$rootScope', 'CorpusNames', function($rootScope, CorpusNames) {
	    return {
		link: function(scope, elem, attrs) {
		    scope.$on('gotFG', function() {
			    if (null == $rootScope.iceData.bg) {
				CorpusNames.getBackground();
			    }
			});
		},
		template: '<div class="badge iceCorpusBadge unselectableText">Background Corpus: {{ iceData.bg }}</div>'
	    }
	}]);

iceApp.directive('serverStatus', ['$rootScope', '$http', '$sce', function($rootScope, $http, $sce) {
	    return {
		link: function(scope, element, attrs) {
		    scope.statusMessage = '';
		    scope.bgColor = '#3BAFDA;'
		    var url = $rootScope.sURL + 'corpus/getSelectedCorpus/?';
		    console.info(url);
		    return $http.get(url).success(function(data, status) {
			    scope.statusMessage = 'Running';
			    scope.statusCode = status;
			    scope.fontSize = 'medium';
			})
			.error(function (data, status) {
				console.error("Can't get selected corpus: " + status);
				if (0 == status) {
				    scope.statusMessage = 'Unavailable';
				    scope.bgColor = '#DA4453';
				    } else if (403 == status) {
				    scope.statusMessage = 'Forbidden';
				    scope.bgcolor = '#ED5565';
				} else {
				    scope.bgColor = '#E9573F';
				}
				scope.statusCode = status;
				scope.fontSize = 'x-small';
			    });
		},
		template: '<div class="serverStatus btn" style="background-color: {{ bgColor }}; font-size={{ fontSize }};">{{ statusCode }}<br />{{ statusMessage }}</div>'
	    };
	}]);

iceApp.directive('focusOn', function() {
	// http://stackoverflow.com/a/18295416
	return function(scope, elem, attr) {
	    scope.$on('focusOn', function(e, name) {
		    if (name === attr.focusOn) {
			elem[0].focus();
		    }
		});
	};
    });

iceApp.filter('rawTerm', function() {
	return function(text) {
	    var lastIndex = text.lastIndexOf('/');
	    return text.substr(0,lastIndex);
	};
    });