var iceApp = angular.module('iceApp');

iceApp.factory("Corpora", ['$http', '$rootScope', function($http, $rootScope) {
	    var cURL = $rootScope.sURL + 'corpus/';

	    var Corpora = {
		data: {
		    fg: '',
		    selected: '',
		    selectedCorpus: {},
		    bg: '',
		    newName: '',
		    list: [],
		    names: []
		},
		cTitleOptions: {
		    'cw': "Count Words",
		    'fp': "Find Patterns",
		    'pr': "Preprocess"
		},
		getNames: function() {
		    var url = cURL + 'getCorpusNames/?';
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    Corpora.data.names = data;
			    $rootScope.$broadcast('Corpora.update');
			})
		    .error(function () {
			    console.error("Can't get corpus names: " + status);
			});
		},
		getList: function() {
		    var url = cURL + 'getAllCorpusInfo/?';
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    Corpora.data.list = data;
			    $rootScope.$broadcast('Corpora.update');
			})
		    .error(function () {
			    console.error("Can't get all corpora: " + status);
			});
		},
		add: function(corpusName, corpusDir, filter) {
		    var url = cURL + 'addCorpus/?corpusName=' + encodeURIComponent(corpusName) + '&dir=' + encodeURIComponent(corpusDir) + '&filter=' + encodeURIComponent(filter);
		    console.info(url);
		    $http.post(url).success(function() {
			    console.log("added corpus " + corpusName);
			    Corpora.getList();
			})
		    .error(function (data, status) {
			    console.error("Add corpus " + corpusName + " from directory " + corpusDir + " with filter " + filter + " failed! Error: " + status);
		    });

		},
		rename: function(newName) {
		    if (newName != null) {
			var url = cURL + 'renameCorpus/?originalName='+encodeURIComponent(Corpora.data.selected)+'&newName='+encodeURIComponent(newName);
			console.info(url);
			return $http.post(url).success(function() {
				Corpora.getList();
			    }).error(function (data, status) {
				    console.error("Can't rename corpus " + originalName + " to " + newName + ": " + status);
				});
		    } else {
			console.error("Can't rename corpus: no new name specified!");
		    }
		},
		delete: function() {
		    if (Corpora.data.selected != null) {
			var url = cURL + 'removeCorpus/?corpusName=' + encodeURIComponent(Corpora.data.selected);
			console.info(url);
			return $http.post(url).success(function() {
				Corpora.getList();
			    })
			.error(function () {
				console.error("Can't delete corpus " + fg + ": " + status);
			    });
		    } else {
			console.error("Can't delete: no corpus selected!");
		    }
		},
		select: function(corpusName) {
		    Corpora.data.selected = corpusName;
		},
		processCorpus: function() {
		    var url = '';
		    if ('fp' == Corpora.data.longProcessName) {
			url = $rootScope.sURL + 'relation/findPatterns/?corpusName=' + encodeURIComponent(Corpora.data.selected);
		    } else if ('cw' == Corpora.data.longProcessName) {
			url = cURL + 'countWords/?corpusName=' + encodeURIComponent(Corpora.data.selected);
		    } else if ('pr' == Corpora.data.longProcessName) {
			url = cURL + 'preprocess/?corpusName=' + encodeURIComponent(Corpora.data.selected);
		    } else {
			console.error("Can't process corpus: no process specified!");
			Corpora.data.okToConfirm = false;
			return;
		    }
		    if (Corpora.data.selected != null) {
			return $http.post(url).success( function() {
				Corpora.data.okToConfirm = false;
				$rootScope.$broadcast('Ops.update');
				Corpora.getList();
			    }).error(function() {
				    console.error("Can't " + Corpora.cTitleOptions + " for corpus " + Corpora.data.selected);
				});
		    } else {
			console.error("Can't " + Corpora.cTitleOptions[Corpora.data.longProcessName] + ": no corpus selected!");
			Corpora.data.okToConfirm = false;
		    }

		},
		refreshInfo: function(corpusName) {
		    var url = cURL + 'refreshCorpusInfo/?corpusName=' + encodeURIComponent(corpusName);
		    console.info(url);
		    return $http.post(url).success(function() {
			    Corpora.getList();
			})
		    .error(function () {
			    console.error("Can't refresh info for corpus " + corpusName + ": " + status);
			});
		    
		},
		confirmProcess: function(longProcessName) {
		    if (null == longProcessName) {
			console.error("Can't show modal: no process name given!");
			return false;
		    }
		    if (null == Corpora.data.selected) {
			console.error("Can't show modal: no corpus selected!");
			return false;
		    }
		    for (i=0; i<Corpora.data.list.length; i++) {
			if (Corpora.data.selected == Corpora.data.list[i].name) {
			    Corpora.data.selectedCorpus = Corpora.data.list[i];
			}
		    }

		    if ('cw' == longProcessName) {
			Corpora.data.processStatus = Corpora.data.selectedCorpus.relationStatus;
		    } else {
			Corpora.data.processStatus = Corpora.data.selectedCorpus.countStatus;
		    }
		    if ("PROCESSING" == Corpora.data.processStatus) {
			console.error("Can't show modal: still processing earlier request!");
			return false;
		    }
		    if ("PROCESSED" == Corpora.data.selectedCorpus.countStatus) {
			Corpora.data.processType = longProcessName + 'p';
		    } else {
			Corpora.data.processType = longProcessName + 'np';
		    }
		    Corpora.data.longProcessName = longProcessName;
		    console.log("confirmProcess(" + longProcessName + ", " + Corpora.data.processStatus + ")");
		    Corpora.data.cTitle = Corpora.cTitleOptions[longProcessName];
		    Corpora.data.okToConfirm = true;
		    $rootScope.$broadcast('Corpora.update');
		    return true;
		}
	    };
	    return Corpora;
	}]);

iceApp.factory("Dirs", ['$http', '$rootScope', function($http, $rootScope) {
	    var cURL = $rootScope.sURL + 'corpus/';

	    var Dirs = {
		data: {
		    selected: '',
		    list: []
		},
		getList: function() {
		    var url = cURL + 'getCorpusDirs/?';
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    Dirs.data.list = data;
			    $rootScope.$broadcast('Dirs.update');
			})
		    .error(function () {
			    console.error("Can't get corpus dirs: " + status);
			});
		},
		select: function(dirName) {
		    Dirs.data.selected = dirName;
		}

	    };
	    return Dirs;
	}]);

iceApp.factory("Entities", ['$http', '$rootScope', 'Corpora', function($http, $rootScope, Corpora) {
	    var cURL = $rootScope.sURL + 'corpus/';

	    var Entities = {
		data: {
		    list: []
		},
		getList: function() {
		    var fg = $rootScope.iceData.fg;
		    var bg = $rootScope.iceData.bg;
		    var url = cURL + 'findEntities/?corpusName=' + encodeURIComponent(fg) + '&backgroundCorpus=' + encodeURIComponent(bg); 
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    Entities.data.list = data;
			    $rootScope.$broadcast('Entities.update');
			})
		    .error(function (data, status) {
			    console.error("Can't get entities: " + status);
			});
		}
	    };
	    return Entities;
	}]);

iceApp.factory("CorpusNames", ['$http', '$rootScope', function($http, $rootScope) {
	    var cURL = $rootScope.sURL + 'corpus/';

	    var CorpusNames = {
		list: [],
		getList: function() {
		    var fg = $rootScope.iceData.fg;
		    var bg = $rootScope.iceData.bg;
		    var url = cURL + 'getCorpusNames/?';
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    CorpusNames.list = data;
			    $rootScope.$broadcast('CorpusNames.update');
			})
		    .error(function (data, status) {
			    console.error("Can't get corpus names: " + status);
			});
		},
		getForeground: function() {
		    var url = cURL + 'getSelectedCorpus/?';
		    console.info(url);
		    return $http.get(url).success(function(data) {
			    $rootScope.iceData.fg = data;
			    $rootScope.$broadcast('gotFG');
			})
		    .error(function () {
			    console.error("Can't get selected corpus: " + status);
		    });
		},
		getBackground: function() {
		    if ($rootScope.iceData.fg != null && $rootScope.iceData.fg.length > 0) {
			var url = cURL + 'getBackgroundCorpus/?corpusName=' + encodeURIComponent($rootScope.iceData.fg);
			console.info(url);
			return $http.get(url).success(function(data) {
				$rootScope.iceData.bg = data;
				$rootScope.$broadcast('gotBG');
			    })
			.error(function (data, status) {
				console.error("Can't get background corpus for corpus " + $rootScope.iceData.fg + ": " + status);
			    });
		    } else {
			console.error("Can't get background corpus: no foreground corpus!");
		    }
		},
		saveForeground: function() {
		    if ($rootScope.iceData.fg != null) {
			var url = cURL + 'setCorpus/?corpusName=' + encodeURIComponent($rootScope.iceData.fg);
			console.info(url);
			return $http.post(url).success(function() {
				CorpusNames.getBackground();
			    })
			.error(function (data, status) {
				console.error("Can't set corpus " + $rootScope.iceData.fg + ": " + status);
			    });
		    } else {
			console.error("Can't set foreground corpus " + $rootScope.iceData.fg);
		    }
		},
		saveBackground: function(bgCorpusName) {
		    if ($rootScope.iceData.fg != null && bgCorpusName != null) {
			var url = cURL + 'setBackgroundCorpus/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&background='+ encodeURIComponent(bgCorpusName);
			console.info(url);
			return $http.post(url).success(function() {
				CorpusNames.getBackground();
			    })
			.error(function (data, status) {
				console.error("Can't set background corpus " + bgCorpusName + " on corpus " + $rootScope.iceData.fg + ": " + status);
			    });
		    } else {
			console.error("Can't set background corpus " + bgCorpusName + " on corpus " + $rootScope.iceData.fg);
		    }
		}
	    };
	    return CorpusNames;
	}]);

iceApp.factory("Indices", ['$http', '$rootScope', 'CorpusNames', function($http, $rootScope, CorpusNames) {
	    var cURL = $rootScope.sURL + 'corpus/';
	    var Indices = {
		data: {
		    selected: '',
		    list: []
		},
		getList: function() {
		    if ($rootScope.iceData.fg != null) {
			var url = cURL + 'getIndices/?corpusName='+ $rootScope.iceData.fg;
			console.info(url);
			return $http.get(url).success(function(data) {
				Indices.data.list = data;
				$rootScope.$broadcast('Indices.update');
			    })
			.error(function () {
				console.error("Can't get indices: " + status);
			    });
		    } else {
			console.error("Can't get index list: no foreground corpus!");
		    }
		},
		add: function(corpusName, type, cutoff) {
		    var url = cURL + 'addIndex/?corpusName=' + encodeURIComponent(corpusName) + '&type=' + encodeURIComponent(type) + '&cutoff=' + encodeURIComponent(cutoff);
		    console.info(url);
		    $http.post(url).success(function() {
			    console.log("Added index " + corpusName + '-' + type + '-' + cutoff);
			    Indices.getList();
			})
		    .error(function (data, status) {
			    console.log("Add index " + corpusName + " for type " + type + " with cutoff " + cutoff + " failed! Error: " + status);
		    });

		},
		delete: function() {
		    if (Indices.data.selected != null) {
			if ($rootScope.iceData.fg != null) {
			    var url = cURL + 'removeIndex/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&indexName=' + encodeURIComponent(Indices.data.selected);
			    console.info(url);
			    return $http.post(url).success(function() {
				    Indices.getList();
				})
			    .error(function () {
				    console.error("Can't delete index " + Indices.data.selected + " from corpus " + $rootScope.iceData.fg + ": " + status);
			    });
			} else {
			    console.error("Can't delete index " + Indices.data.selected + ": no corpus selected!");
			}
		    } else {
			console.error("Can't delete: no index selected!");
		    }
		},
		select: function(indexName) {
		    Indices.data.selected = indexName;
		    $rootScope.$broadcast('Indices.update');
		}
	    };
	    return Indices;
	}]);

iceApp.factory("EntitySets", ['$http', '$rootScope', function($http, $rootScope) {
  var cURL = $rootScope.sURL + 'entitySet/';
  var EntitySets = {
    data: {
    downloadURL: cURL + 'export/',
    selected: '',
    selectedEntity: '',
    list: [],
    ranking: {},
    expansion: [],
    expander: '',
    expandStatus: []
  },
  getList: function() {
    var url = cURL + 'getEntitySets/?';
    console.info(url);
    return $http.get(url).success(function(data) {
	    EntitySets.data.list = data;
	    $rootScope.$broadcast('EntitySets.update');
      })
      .error(function (data, status) {
	      console.error("Can't get entity sets: " + status);
    });
  },
  add: function(entitySetName) {
    var url = cURL + 'addEntitySet/?entitySetName=' + encodeURIComponent(entitySetName);
    console.info(url);
    return $http.post(url).success(function(data) {
	    EntitySets.getList();
	})
    .error(function (data, status) {
      console.error("Can't add entity set " + entitySetName + ": " + status);
	});
      },
  delete: function() {
    if (EntitySets.data.selected != null) {
      var entitySetName = EntitySets.data.selected;
      var url = cURL + 'removeEntitySet/?entitySetName=' + encodeURIComponent(entitySetName);
      console.info(url);
      return $http.post(url).success(function(data) {
	      EntitySets.getList();
	  })
      .error(function (data, status) {
	 onsole.error("Can't remove entity set " + entitySetName + ": " + status);
	  });
    } else {
	console.error("Can't remove entity set: no set selected!");
    }
      },
  suggestSeeds: function(indexName) {
    if (EntitySets.data.selected != null) {
      if ($rootScope.iceData.fg != null) {
        var entitySetName = EntitySets.data.selected;
	var url = cURL + 'suggestEntitySetSeeds/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&indexName=' + encodeURIComponent(indexName) + '&entitySetName=' + encodeURIComponent(entitySetName);
	console.info(url);
	return $http.post(url).success(function(data) {
	  EntitySets.getList().then( function() {
	    EntitySets.select(entitySetName);
	  });
	})
	.error(function (data, status) {
	   console.error("Can't suggest seeds for entity set " + entitySetName + " in corpus " + $rootScope.iceData.fg + " with index " + indexName + ": " + status);
	});
      } else {
	  console.error("Can't suggest seeds for entity set " + entitySetName + ": no corpus selected!");
      }
    } else {
	console.error("Can't suggest seeds: no entity set selected!");
    }

  },
  expand: function(indexName) {
    if (EntitySets.data.selected != null && EntitySets.data.selected.length > 0) {
      if ($rootScope.iceData.fg != null) {
	var entitySetName = EntitySets.data.selected;
      	var url = cURL + 'expand/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&indexName=' + encodeURIComponent(indexName) + '&entitySetName=' + encodeURIComponent(entitySetName);
	console.info(url);
	return $http.post(url).success(function(data) {
	  EntitySets.data.expander = data;
	  EntitySets.data.expandStatus.progress = 0;
	  $rootScope.$broadcast('EntitySets.expanding');
				    
	})
	.error(function (data, status) {
	  console.error("Can't expand entity set " + entitySetName + " based on corpus " + $rootScope.iceData.fg + " with index " + indexName + ": " + status);
	  $rootScope.$broadcast('EntitySets.abort');
	    });
      } else {
	  console.error("Can't expand entity set " + entitySetName + ": no corpus selected!");
      }
    } else {
	console.error("Can't expand entity set: no set selected!");
    }
  },
  expandStatus: function() {
    if (EntitySets.data.expander.length > 0) {
      var expander = EntitySets.data.expander;
      var url = $rootScope.sURL + 'operation/onProgressChange/?operationId=' + encodeURIComponent(EntitySets.data.expander);
      //      console.info(url);
      return $http.get(url).success(function(data) {
	EntitySets.data.expandStatus = data;
	$rootScope.$broadcast('EntitySets.expanding');
      })
      .error(function (data, status) {
	console.error("Can't get status of expansion "+ expander +': '+status);
      // TODO onProgressComplete
	url = $rootScope.sURL + 'operation/onProgressComplete/?operationId=' + encodeURIComponent(expander);
	console.info(url);
        return $http.get(url).success(function(data) {
	  EntitySets.data.expandStatus = data;
	  $rootScope.$broadcast('EntitySets.expanding');
        }).error(function (data, status) {
	  console.error("Can't get completion of expander "+ expander + ': ' + status); 
	    });
      });
    } else {
	console.error("Can't get status of expansion: no expander present!");
    }
  },
  getRanked: function() {
    if (EntitySets.data.expander.length > 0) {
      if (EntitySets.data.selected != null) {
	var expander = EntitySets.data.expander;
	var url = cURL + 'getRanked/?operationId=' + encodeURIComponent(EntitySets.data.expander) + '&entitySetName=' + encodeURIComponent(EntitySets.data.selected);
	console.info(url);
	return $http.get(url).success(function(data) {
	  EntitySets.data.ranking = data;
	  var i = 0;
	  for (e in data) {
	      i++;
	  }
	  $rootScope.$broadcast('EntitySets.expanded');
	    })
	.error(function (data, status) {
	   console.error("Can't get status of expansion " + expander + ': ' + status);
	    });
      } else {
	  console.error("Can't get entity set expansion: no entity set selected!");
      }
    } else {
	console.error("Can't get entity set expansion: no expander present!");
    }
  },

  rank: function(indexName) {
    if (EntitySets.data.selected != null) {
      if ($rootScope.iceData.fg != null) {
        var entitySetName = EntitySets.data.selected;
	var url = cURL + 'rank/?entitySetName=' + encodeURIComponent(EntitySets.data.selected) + '&operationId=' + encodeURIComponent(EntitySets.data.expander) + '&corpusName=' + encodeURIComponent($rootScope.iceData.fg);
	console.info(url);
	return $http.post(url, EntitySets.data.ranking).success(function(data) {
	  EntitySets.data.expander = data;
	  EntitySets.data.expandStatus.progress = 0;
	  $rootScope.$broadcast('EntitySets.expanding');
	})
	.error(function (data, status) {
	  console.error("Can't rank entity set " + entitySetName + " based on corpus " + $rootScope.iceData.fg + " with index " + indexName + ": " + status);
	  $rootScope.$broadcast('EntitySets.abort');
	});
      } else {
	console.error("Can't rank entity set " + entitySetName + ": no corpus selected!");
      }
    } else {
	console.error("Can't rank entity set: no set selected!");
    }
    
  },
  saveRanking: function() {
    if (EntitySets.data.selected != null) {
      if ($rootScope.iceData.fg != null) {
	var entitySetName = EntitySets.data.selected;
	var url = cURL + 'saveRanked/?entitySetName=' + encodeURIComponent(entitySetName);
	console.info(url);
	return $http.post(url, EntitySets.data.ranking).success(function() {
		EntitySets.getList().then( function() {
			EntitySets.select(entitySetName);
		    });
	    })
	.error(function (data, status) {
	  console.error("Can't rank entity set " + entitySetName + " based on corpus " + $rootScope.iceData.fg + " with index " + indexName + ": " + status);
	  $rootScope.$broadcast('EntitySets.abort');
	});
      } else {
        console.error("Can't rank entity set " + entitySetName + ": no corpus selected!");
      }
    } else {
      console.error("Can't rank entity set: no set selected!");
    }

  },

  select: function(entitySetName) {
    EntitySets.data.selected = entitySetName;
    if (EntitySets.data.list[entitySetName].nouns.length > EntitySets.data.list[entitySetName].names.length ) {
      EntitySets.data.entities = EntitySets.data.list[entitySetName].nouns;
    } else {
      EntitySets.data.entities = EntitySets.data.list[entitySetName].names;
    }
    $rootScope.$broadcast('EntitySets.update');
  },

  selectEntity: function(entityName) {
    EntitySets.data.selectedEntity = entityName;
    $rootScope.$broadcast('EntitySets.update');
  },

  addEntity: function(entityName) {
    if (EntitySets.data.selected != null & EntitySets.data.selected.length > 0) {
      var entitySetName = EntitySets.data.selected;
      var url = cURL + 'addEntity/?entitySetName=' + encodeURIComponent(entitySetName) + '&entity=' + encodeURIComponent(entityName);
      console.info(url);
      return $http.post(url).success(function(data) {
	EntitySets.getList().then( function() {
	  EntitySets.select(entitySetName);
	});
      })
      .error(function (data, status) {
	console.error("Can't add entity " + entityName + " to entity set " + entitySetName + ": " + status);
      });
    } else {
      console.error("Can't add entity " + entityName + ": no entity set selected!");
    }
  },

  deleteEntity: function(entityName) {
    if (EntitySets.data.selected != null) {
      var entitySetName = EntitySets.data.selected;
      var url = cURL + 'removeEntity/?entitySetName=' + encodeURIComponent(entitySetName) + '&entity=' + encodeURIComponent(entityName);
      console.info(url);
      return $http.post(url).success(function(data) {
	EntitySets.getList().then( function() {
	  EntitySets.select(entitySetName);
	});
      })
      .error(function (data, status) {
	console.error("Can't remove entity " + entityName + " from entity set " + entitySetName + ": " + status);
      });
    } else {
      console.error("Can't remove entity " + entityName + ": no entity set selected!");
    }
  },

  export: function() {
    var url = cURL + 'buildTypes/?';
    console.info(url);
    return $http.post(url).success(function(data) {
      $rootScope.$broadcast('EntitySets.exported');
    })
    .error(function (data, status) {
      console.error("Can't export entity sets: " + status);
    });
  }
};
  return EntitySets;
}]);

iceApp.factory("Relations", ['$http', '$rootScope', function($http, $rootScope) {
  var cURL = $rootScope.sURL + 'relation/';
  var Relations = {
      data: {
	  downloadURL: cURL + 'download/',
	  selected: '',
	  list: {},
	  expansion: []
      },
      getList: function() {
	  if ($rootScope.iceData.fg != null) {
	      var url = cURL + 'getRelationNames/?corpusName=' + encodeURIComponent($rootScope.iceData.fg);
	      console.info(url);
	      return $http.get(url).success(function(data) {
		      Relations.data.list = data;
		      $rootScope.$broadcast('Relations.update');
		  })
	      .error(function (data, status) {
		      console.error("Can't get relation names: " + status);
		  });
	  } else {
	      console.error("Can't get relation list: no foreground corpus!");
	  }
      },
      addPattern: function(relationName, pattern) {
	  if ($rootScope.iceData.fg != null) {
	      var url = cURL + 'addToRelation/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(relationName) + '&relationInstance=' + encodeURIComponent(pattern);
	      console.info(url);
	      return $http.post(url).success(function(data) {
		      Relations.getList();
		  })
	      .error(function (data, status) {
		      console.error("Can't add pattern " + pattern + " to relation " + relationName + ": " + status);
		  });
	  } else {
	      console.error("Can't add pattern to relation: no foreground corpus!");
	  }
      },
      rename: function(newName) {
	  if ($rootScope.iceData.fg != null) {
	      var relationName = Relations.data.selected;
	      var url = cURL + 'renameRelation/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&originalName=' + encodeURIComponent(relationName) + '&newName=' + encodeURIComponent(newName);
	      console.info(url);
	      return $http.post(url).success(function(data) {
		      Relations.getList();
		  })
	      .error(function (data, status) {
		      console.error("Can't delete relation " + relationName + ": " + status);
		  });
	  } else {
	      console.error("Can't delete relation: no foreground corpus!");
	  }
      },
      delete: function() {
	  if ($rootScope.iceData.fg != null) {
	      var relationName = Relations.data.selected;
	      var url = cURL + 'removeRelation/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(relationName);
	      console.info(url);
	      return $http.post(url).success(function(data) {
		      Relations.getList();
		  })
	      .error(function (data, status) {
		      console.error("Can't delete relation " + relationName + ": " + status);
		  });
	  } else {
	      console.error("Can't delete relation: no foreground corpus!");
	  }
      },
      expand: function() {
	  if ($rootScope.iceData.fg != null) {
	      if (Relations.data.selected != null) {
		  var url = cURL + 'expand/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(Relations.data.selected);
		  console.info(url);
		  return $http.post(url).success(function(data) {
			  Relations.data.expansion = data;
			  $rootScope.$broadcast('Relations.expanded');
		      })
		  .error(function(data, status) {
			  console.error("Can't expand relation " + Relations.data.selected + " using corpus " + $rootScope.iceData.fg + ": " + status);
		      });
	      } else {
		  console.error("Can't expand relation: no relation selected!");
	      }
	  } else {
	      console.error("Can't expand relation: no foreground corpus!");
	  }
      },
      rank: function() {
	  if (Relations.data.selected != null) {
	      if ($rootScope.iceData.fg != null) {
		  var relationName = Relations.data.selected;
		  var url = cURL + 'rankRelation/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(relationName);
		  var approvedPaths = [];
		  var rejectedPaths = [];
		  var r = {};
		  for (i=0; i<Relations.data.expansion.length; i++) {
		      r = Relations.data.expansion[i];
		      if (r.userAssess === 'y') {
			  approvedPaths.push(r);
		      } else if (r.userAssess === 'n') {
			  rejectedPaths.push(r);
		      }
		  }
		  console.info(url);
		  return $http.post(url, [approvedPaths, rejectedPaths]).success(function(data) {
			  Relations.data.expansion = data;
			  $rootScope.$broadcast('Relations.expanded');
		      })
		  .error(function (data, status) {
			  console.error("Can't rank patterns in " + relationName + " based on corpus " + $rootScope.iceData.fg + ": " + status);
			  $rootScope.$broadcast('Relations.abort');
		      });
	      } else {
		  console.error("Can't rank patterns in relation " + relationName + ": no corpus selected!");
	      }
	  } else {
	      console.error("Can't rank patterns in relation: no relation selected!");
	  }
	  
      },
      saveRanking: function() {
	  if (Relations.data.selected != null) {
	      if ($rootScope.iceData.fg != null) {
		  var relationName = Relations.data.selected;
		  var url = cURL + 'saveRelation/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(relationName);
		  var approvedPaths = [];
		  var r = {};
		  for (i=0; i<Relations.data.expansion.length; i++) {
		      r = Relations.data.expansion[i];
		      if (r.userAssess === 'y') {
			  approvedPaths.push(r);
		      }
		  }
		  console.info(url);
		  return $http.post(url, approvedPaths).success(function() {
			  Relations.getList().then( function() {
				  $rootScope.$broadcast('Relations.update');
			      });
		      })
		  .error(function (data, status) {
			  console.error("Can't save pattern ranks for relation " + relationName + " based on corpus " + $rootScope.iceData.fg + ": " + status);
			  $rootScope.$broadcast('Relations.abort');
		      });
	      } else {
		  console.error("Can't save pattern ranks for relation " + relationName + ": no corpus selected!");
	      }
	  } else {
	      console.error("Can't save pattern ranks: no relation selected!");
	  }
      },
    removePath: function(fullPath) {
      if (Relations.data.selected != null && Relations.data.selected.length>0){
	  var url = cURL + 'removePath/?relationName=' + encodeURIComponent(Relations.data.selected) + '&fullPath=' + encodeURIComponent(fullPath);
	  console.info(url);
	  return $http.post(url).success(function() {
		      $rootScope.$broadcast('Relations.update');
	      }).error(function(data, status) {
		      console.error("Can't remove path " + fullPath + " from relation " + Relations.data.selected + ": " + status);
	      });
      }
    },

      export: function() {
	  if (Relations.data.selected != null) {
	      if ($rootScope.iceData.fg != null) {
		  var relationName = Relations.data.selected;
		  var url = cURL + 'export/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&relationName=' + encodeURIComponent(Relations.data.selected);
		  console.info(url);
		  return $http.post(url).success(function() {
			  $rootScope.$broadcast('Relations.exported');
		      })
		  .error(function (data, status) {
			  console.error("Can't export relation " + relationName + " based on corpus " + $rootScope.iceData.fg + ": " + status);
		      });
	      } else {
		  console.error("Can't export relation " + relationName + ": no corpus selected!");
	      }
	  } else {
	      console.error("Can't export relation: no relation selected!");
	  }
	  
      }

  };
  return Relations;
	}]);

iceApp.factory("RelationInfo", ['$http', '$rootScope', function($http, $rootScope) {
  var cURL = $rootScope.sURL + 'relation/';
  var RelationInfo = {
    data: {
      info: {},
      patterns: {},
      selected: {},
      selectedPattern: {},
      show: 'all'
    },
    clearSelected: function() {
      RelationInfo.data.selected = '';
      RelationInfo.data.selectedPattern = '';
    },

    get: function(name) {
      if ($rootScope.iceData.fg != null && $rootScope.iceData.fg.length > 0) {
	  if (name != null && name.length > 0) {
	      var url = cURL + 'getRelationInfo/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&name=' + encodeURIComponent(name);
	      console.info(url);
	      return $http.get(url).success(function(data) {
		      console.log(data);
		      RelationInfo.clearSelected();
		      RelationInfo.data.info = data;
		      RelationInfo.data.show = 'relation';
		      $rootScope.$broadcast('RelationInfo.update');
		  })
	      .error(function (data, status) {
		console.error("Can't get relationInfo: for relation " + name + ' in corpus ' + $rootScope.iceData.fg + ': ' + status);
		  });
	  } else {
	      console.error("Can't get relationInfo: no relation name given!");
	  }
      } else {
	  console.error("Can't get relationInfo: no foreground corpus given!");
      }
    },

    getPatterns: function(filterName) {
		    if ($rootScope.iceData.fg != null) {
			var url = cURL + 'getPatterns/?corpusName=' + encodeURIComponent($rootScope.iceData.fg) + '&filter=' + encodeURIComponent(filterName);
			console.info(url);
			return $http.get(url).success(function(data) {
				RelationInfo.clearSelected();
				RelationInfo.data.patterns = data;
				RelationInfo.data.show = 'all';
				$rootScope.$broadcast('RelationInfo.update');
			    })
			.error(function(data, status) {
				console.error("Can't get patterns for corpus " + $rootScope.iceData.fg + " with filter " + filterName + ": " + status);
			    });
		    } else {
			console.error("Can't get patterns: no corpus selected!");
		    }
		},
		select: function(path) {
		    RelationInfo.clearSelected();
		    RelationInfo.data.selected = path;
		},
		selectPattern: function(pattern) {
		    RelationInfo.clearSelected();
		    RelationInfo.data.selectedPattern = pattern;
		}
	    };
	    return RelationInfo;
	}]);

iceApp.factory("Operations", ['$http', '$rootScope', function($http, $rootScope) {
  var cURL = $rootScope.sURL + 'operation/';

  var Operations = {
      data: {
	  ops: {},
	  pending: {},
	  pendingTotal: 0,
	  completed: {},
	  completedTotal: 0
      },
      nextStep: {
	  'Count Words': {
	      name: 'View Entities',
	      page: 'viewEntities'
	  },
	  'Index Corpus': {
	      name: 'Edit Entity Sets',
	      page: 'editEntitySets'
	  },
	  'Find Patterns': {
	      name: 'Edit Relations',
	      page: 'editRelations'
	  }
      },
      getList: function() {
	  var url = cURL + 'all/?';
	  // console.info(url);
	  $http.get(url).success( function(data) {
		  Operations.data.pending = {};
		  Operations.data.pendingTotal = 0;
		  Operations.data.completed = {};
		  Operations.data.completedTotal = 0;
		  Operations.data.ops = data;
		  angular.forEach(Operations.data.ops, function(value, key) {
			  if (value.alive) {
			      Operations.data.pending[key] = value;
			      Operations.data.pendingTotal++;
			  } else {
			      Operations.data.completed[key] = value;
			      Operations.data.completedTotal++;
			  }
		      });
		  $rootScope.$broadcast('Ops.update');
	      }).error( function(data, status) {
		      console.error("Can't get operations: " + status);
		  });
      }
  };
  return Operations;

	}]);

iceApp.factory('Focus', function($rootScope, $timeout) {
	return function(name) {
	    $timeout(function() {
		    $rootScope.$broadcast('focusOn', name);
		});
	};
    });
