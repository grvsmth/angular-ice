# Angular ICE #

Angular ICE is a frontend to [the RESTful ICE server](https://github.com/rgrishman/ice), written in AngularJS and Bootflat.

### How do I get set up? ###

* Set up and run the RESTful ICE server
* Install these files on a web server
* Install the latest versions of AngularJS and Bootflat
* Edit js/iceApp.js to point to the RESTful ICE server
* Test by installing a corpus, preprocessing, creating indices, etc.

### Acknowledgments ###

Angular ICE is based on the ICE frontend created by Joel Sieh for SRI.

### Copyright ###
Angular ICE is licensed under [the Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

### Creator ###

Angus B. Grieve-Smith  
Computer Science Department  
New York University